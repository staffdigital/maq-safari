$(function(){
	$('.soloNumber').each((k, el)=>{
		$(el).on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
	// +++ menu responsive +++
	//clone div's desktop to menu resposnsive 
	$('.header-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('header-list');
	$('.header-logo').clone().prependTo('.menu-sidebar-cnt').removeClass('header-logo').addClass('responsive-logo');
	$('.header__account').clone().appendTo('.menu-sidebar-cnt');

	//events: menu burguer
	function cerrar_nav() {
		$('.menu-mobile-open').removeClass('active');
		$('.menu-mobile-close').removeClass('active');
		$('.menu-sidebar').removeClass('active');
		$('.header-menu-overlay').removeClass('active');
		$('body').removeClass('active');
	};
	function abrir_nav(){
		$('.menu-mobile-open').addClass('active');
		$('.menu-mobile-close').addClass('active');
		$('.menu-sidebar').addClass('active');
		$('.header-menu-overlay').addClass('active');
		$('body').addClass('active');
	}
	$('.menu-mobile-close , .header-menu-overlay').click(function(event) {
		event.preventDefault();
		cerrar_nav();
	});
	$('.menu-mobile-open').click(function(event) {
		abrir_nav()
	});

	//detectando tablet, celular o ipad
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	  // tasks to do if it is a Mobile Device
	  function readDeviceOrientation() {
		if (Math.abs(window.orientation) === 90) {
			// Landscape
			cerrar_nav();
		} else {
			// Portrait
			cerrar_nav();
		}
	  }
	  window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function() {
			var estadomenu = $('.menu-responsive').width();
			if(estadomenu != 0){
				cerrar_nav();
			}
		});
	}
	// --- end ---

	// +++ header scroll +++
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		if (altoScroll > 0) {
			$('.header').addClass('header--scroll');
		}else{
			$('.header').removeClass('header--scroll');
		};
	});
	// --- end ---
	
	// +++ hover header menu +++
	var header__menu = $(window).innerWidth();
	console.log(header__menu);
	if (header__menu > 1024) {
		$('.header .header__submenu--show').hover(function() {
			$('.header').addClass('header__menu--hover');
			$('html, body').addClass('header--overflowhidden')
		}, function() {
			$('.header').removeClass('header__menu--hover');
			$('html, body').removeClass('header--overflowhidden')

		});
	}
	// --- end ---

	// +++ search +++
	$('.header__search__open').click(function(event){
		event.preventDefault();
		$('.header-search-pop-up').addClass('active');
		$('.header-overlay').addClass('active');
		$('body, html').addClass('header--overflowhidden');
	});
	$('.h-cerrar, .header-overlay').click(function(event){
		event.preventDefault();
		$('.header-search-pop-up').removeClass('active');
		$('.header-overlay').removeClass('active');
		$('body, html').removeClass('header--overflowhidden');
	});
	// --- end ---

	$('.menu-responsive .header__submenu--show .header__menu__link').click(function(e){
		e.preventDefault();
		if ($(this).parent().hasClass('active__toggle')) { // ¿si tiene active?
			$(this).parent().removeClass('active__toggle').find('.header__submenu').stop().slideUp(); // remuevo active, subo un nivel, busco, realizo stop y cierro toggle
		}else{
			$('.menu-responsive .header__menu__link').parent().removeClass('active__toggle'); // ¿si no tiene active? remuevo active de todos 			
			$('.menu-responsive .header__submenu').stop().slideUp(); // cierro toggle de todos			
			$(this).parent().addClass('active__toggle').find('.header__submenu').stop().slideToggle(); // de quien hice click agrego active, busco, realizo stop y abro toogle
		};
	});

	$('.menu-responsive .menu__submenu__title__icono').click(function(e){
		e.preventDefault();
		if ($(this).parent().parent().hasClass('active__toggle')) { // ¿si tiene active?
			$(this).parent().parent().removeClass('active__toggle').find('.header__submenu__links').stop().slideUp(); // remuevo active, subo un nivel, busco, realizo stop y cierro toggle
		}else{
			$('.menu-responsive .menu__submenu__title__icono').parent().parent().removeClass('active__toggle'); // ¿si no tiene active? remuevo active de todos 			
			$('.menu-responsive .header__submenu__links').stop().slideUp(); // cierro toggle de todos			
			$(this).parent().parent().addClass('active__toggle').find('.header__submenu__links').stop().slideToggle(); // de quien hice click agrego active, busco, realizo stop y abro toogle
		};
	});

	// validation
	// $(".equipo__form form").validationEngine();
});